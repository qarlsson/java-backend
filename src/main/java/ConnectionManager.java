import spark.Request;
import spark.Response;
import java.sql.*;

/**
 * Created by yermolaev on 28.10.15.
 */
public class ConnectionManager {

    public Connection conn;
    private Statement st;
    public ConnectionManager(String adress, String user, String password) {
        try {
            conn = DriverManager.getConnection("jdbc:postgresql:" + adress + "?user=" + user + "&password=" + password);
            st = conn.createStatement();
            //System.out.println("success");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean login(Request req, Response resp) {
        String request = "SELECT *\n FROM users\n WHERE username='"
                + req.params(":login") + "' AND password='" + req.params(":password") + "';";
        resp.header("Access-Control-Allow-Origin", "*");
        try {
            ResultSet rs = st.executeQuery(request);
            return rs.isBeforeFirst();
        } catch (Exception e) {
            return false;
        }
    }

    public String getTables(Request req, Response resp) {
        String request = "SELECT table_name\n" +
                "FROM information_schema.tables\n" +
                "WHERE table_schema = 'public';";
        resp.header("Access-Control-Allow-Origin", "*");
        return executeQuery1(request);
    }

    public String getAllAttributes(Request req, Response resp) {
        String request = "SELECT DISTINCT column_name\n" +
                "FROM information_schema.\"columns\"\n" +
                "WHERE \"table_name\" IN ('publication', 'authored','isbn', 'person');";
        resp.header("Access-Control-Allow-Origin", "*");
        return executeQuery2(request);
    }

    public String getTableAttributes(Request req, Response resp) {
        String request = "SELECT column_name\n" +
            "FROM information_schema.\"columns\"\n" +
            "WHERE \"table_name\"='" + req.params(":table") + "';";
        resp.header("Access-Control-Allow-Origin", "*");
        return executeQuery2(request);
    }

    public boolean editRecord(Request req, Response resp) {
        String request = "UPDATE " + req.params(":table") + " SET " + req.params(":attributeName")
                + "='" + req.params(":newAttributeValue") + "'\nWHERE key='" + req.params(":key").replaceAll("!", "/") + "';";
        resp.header("Access-Control-Allow-Origin", "*");
        try {
            ResultSet rs = st.executeQuery(request);
            return true;
        } catch (org.postgresql.util.PSQLException e) {
            e.printStackTrace();
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    public void deleteRecord(Request req, Response resp) {
        String request = "DELETE FROM " + req.params(":table") + "\n" +
                "WHERE pubid=" + req.params(":key") + ";";
        System.out.println(request);
        resp.header("Access-Control-Allow-Origin", "*");
        try {
            st.executeQuery(request);
        } catch (Exception e) {
            //return "Something is wrong...";
        }
    }

    public String search(Request req, Response resp) {
        String request = "SELECT * \n" +
                "FROM " + req.params(":table") + "\n" +
                "WHERE LOWER(" +  req.params(":attribute") +
                //")=LOWER('" + req.params(":value") + "');";
                ") LIKE LOWER('%" + req.params(":value") + "%');";
        System.out.println(request);
        resp.header("Access-Control-Allow-Origin", "*");
        return executeQuery(request);
    }


    public String executeQuery(String query) {
        try {
            ResultSet rs = st.executeQuery(query);
            int columnNumber = rs.getMetaData().getColumnCount();
            StringBuilder result = new StringBuilder();

            while (rs.next()) {
                for (int i = 1; i <= columnNumber-1; i++) {
                    if (rs.getString(i) != null)
                        result.append(rs.getString(i));
                    result.append(",");
                }
                result.append(rs.getString(columnNumber));
                result.append("\n");
            }
            return result.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public String executeQuery1(String query) {
        try {
            ResultSet rs = st.executeQuery(query);
            int columnNumber = rs.getMetaData().getColumnCount();
            StringBuilder result = new StringBuilder();
            while (rs.next()) {
                for (int i = 1; i <= columnNumber; i++) {
                    result.append("<option value=\"");
                    if (rs.getString(i) != null)
                        result.append(rs.getString(i));
                    result.append("\">");
                    if (rs.getString(i) != null)
                        result.append(rs.getString(i));
                    result.append("</option>");
                }
            }
            return result.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String executeQuery2(String query) {
        try {
            ResultSet rs = st.executeQuery(query);
            int columnNumber = rs.getMetaData().getColumnCount();
            StringBuilder result = new StringBuilder();
           while (rs.next()) {
                for (int i = 1; i <= columnNumber; i++) {
                    if (rs.getString(i) != null)
                        result.append(rs.getString(i));
                    result.append(" ");
                }
            }
            return result.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


}
