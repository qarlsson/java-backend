import static spark.Spark.*;

/**
 * Created by yermolaev on 28.10.15.
 */
public class RouteManager {

    public static void main(String[] args) {

        ConnectionManager connManager = new ConnectionManager(args[0], args[1], args[2]);

        get("/edit/:table/:key/:attributeName/:newAttributeValue", (req, res) -> {
            return connManager.editRecord(req, res);
        });

        get("/search", (req, res) -> {
            String a =  connManager.getTables(req, res);
            if (a == null)
                return "null";
            return a;
        });
        get("/search/:table", (req, res) -> {
            String a =  connManager.getTableAttributes(req, res);
            if (a == null)
                return "null";
            return a;
        });

        get("/attributes", (req, res) -> {
            String a =  connManager.getAllAttributes(req, res);
            if (a == null)
                return "null";
            return a;
        });

        get("login/:login/:password", (req, res) -> {
            boolean answer = connManager.login(req, res);
            if (answer) {
                return 1;
            } else {
                return 0;
            }

        });

        get("/search/:table/:attribute/:value", (req, res) ->{
            String result = connManager.search(req, res);
            if (result == null)
                return "Something is wrong...";
            return result;
        });

        get("delete/:table/:key", ((request, response) -> {
            connManager.deleteRecord(request, response);
            return 1;
        }));

    }

}
