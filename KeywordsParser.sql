﻿CREATE OR REPLACE FUNCTION make_keywords()
	RETURNS VOID AS
$$ 
DECLARE 
	publication_r RECORD;
	splited TEXT[];
	word TEXT;
BEGIN
	EXECUTE 'CREATE TABLE keywords(
		id SERIAL PRIMARY KEY,
		keyword TEXT);';

	FOR publication_r IN SELECT title FROM publication LOOP
		splited = regexp_split_to_array(publication_r.title, ' ');
		FOREACH word IN ARRAY splited LOOP
			word := regexp_replace(word, '[^\w\s]', '');
			IF char_length(word) > 4 AND word NOT IN (SELECT keyword FROM keywords) THEN
				INSERT INTO keywords VALUES (DEFAULT, word);
			END IF;
		END LOOP;
	END LOOP;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION match_keywords() 
	RETURNS VOID AS
$$ DECLARE 
	publication_r RECORD;
	splited TEXT[];
	word TEXT;
	current INTEGER;
	prev INTEGER;
BEGIN
	EXECUTE 'CREATE TABLE has_keywords(
		pub_id INTEGER,
		keyword_id INTEGER,
		PRIMARY KEY(pub_id, keyword_id));';

	prev := -1;
	FOR publication_r IN SELECT pubid, title FROM publication LOOP
		splited = regexp_split_to_array(publication_r.title, ' ');
		FOREACH word IN ARRAY splited LOOP
			word := regexp_replace(word, '[^\w\s]', '');
			word := regexp_replace(word, '''', '');
			IF char_length(word) > 4 THEN
				EXECUTE format('SELECT id FROM keywords WHERE keyword = ''%s'' LIMIT 1', word) INTO current;
				IF (SELECT pub_id FROM has_keywords WHERE pub_id = publication_r.pubid AND keyword_id = current) IS NULL 
					AND current IS NOT NULL THEN
					INSERT INTO has_keywords VALUES(publication_r.pubid, current);
				END IF;
				prev := current;
			END IF;
		END LOOP;
	END LOOP;
END; $$
LANGUAGE PLPGSQL;

SELECT make_keywords();
SELECT match_keywords();